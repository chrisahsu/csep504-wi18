import {expect} from "chai";
import {EngExp, EngExpError} from "../src/engexp";

describe("custom", () => {
    it("test five new operators", () => {
        const ee1 = new EngExp()
            .beginCapture()
            .match("abc")
            .endCapture();

        const ee2 = new EngExp()
            .beginLevel()
            .match("123")
            .endLevel();

        const e1 = ee1.asRegExp();
        const e2 = ee2.asRegExp();
        const e3 = ee2.or(ee1).asRegExp();

        var result = e1.exec("abc");
        expect(result[1]).to.be.equal("abc");

        result = e2.exec("123");
        expect(result.length).to.be.equal(1);

        result = e3.exec("abc");
        expect(result[1]).to.be.equal("abc");

        result = e3.exec("123");
        expect(result[1]).to.be.undefined;

        expect(e3.test("abc")).to.be.true;
        expect(e3.test("123")).to.be.true;
        expect(e3.test("abc123")).to.be.true;
        expect(e3.test("ab12c3")).to.be.false;
    });

    it("should throw error for unopened or unclosed levels", () => {
        function createMalformed1(flags) {
            return new EngExp()
                .beginLevel()
                .asRegExp();
        }

        function createMalformed2(flags) {
            return new EngExp()
                .beginLevel()
                .endLevel()
                .endLevel()
                .asRegExp();
        }

        function createMalformed3(flags) {
            return new EngExp()
                .endLevel()
                .asRegExp();
        }

        expect(createMalformed1).to.throw(EngExpError);
        expect(createMalformed2).to.throw(EngExpError);
        expect(createMalformed3).to.throw(EngExpError);
    });

    it("should throw error for unopened or unclosed capture levels", () => {
        function createMalformed1(flags) {
            return new EngExp()
                .beginCapture()
                .asRegExp();
        }

        function createMalformed2(flags) {
            return new EngExp()
                .beginCapture()
                .endCapture()
                .endCapture()
                .asRegExp();
        }

        function createMalformed3(flags) {
            return new EngExp()
                .endCapture()
                .asRegExp();
        }

        expect(createMalformed1).to.throw(EngExpError);
        expect(createMalformed2).to.throw(EngExpError);
        expect(createMalformed3).to.throw(EngExpError);
    });

    it("endLevel cannot close beginCapture, endCapture cannot close beginLevel", () => {
        function createMalformed1(flags) {
            return new EngExp()
                .beginCapture()
                .endLevel()
                .asRegExp();
        }

        function createMalformed2(flags) {
            return new EngExp()
                .beginLevel()
                .endCapture()
                .asRegExp();
        }

        function createMalformed3(flags) {
            return new EngExp()
                .beginLevel()
                .beginCapture()
                .endLevel()
                .endCapture()
                .asRegExp();
        }

        expect(createMalformed1).to.throw(EngExpError);
        expect(createMalformed2).to.throw(EngExpError);
        expect(createMalformed3).to.throw(EngExpError);
    });

    it("endLevel, endCapture only work for beginLevel, beginCapture on the same level", () => {
        function createMalformed1(flags) {
            return new EngExp()
                .beginCapture()
                .then(new EngExp()
                    .endCapture()
                )
                .asRegExp();
        }

        function createMalformed2(flags) {
            return new EngExp()
                .beginLevel()
                .then(new EngExp()
                    .endLevel()
                )
                .asRegExp();
        }

        expect(createMalformed1).to.throw(EngExpError);
        expect(createMalformed2).to.throw(EngExpError);
    });
});
