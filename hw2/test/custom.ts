import {expect} from "chai";

import * as q from "../src/q";
const Q = q.Q;
import * as queries from "../src/queries";

import {crimeData} from "./data";
const rawData = crimeData.data;

function flattenThenNodes(query) {
    if (query instanceof q.ThenNode) {
        return flattenThenNodes(query.first).concat(flattenThenNodes(query.second));
    } else if (query instanceof q.IdNode) {
        return [];
    } else {
        return [query];
    }
}

function expectQuerySequence(query, seq) {
    const flat = flattenThenNodes(query);
    expect(flat.length).to.be.equal(seq.length);
    for (let i = 0; i < flat.length; i++) {
        expect(flat[i].type).to.be.equal(seq[i]);
    }
}

// You should provide at least 5 tests, using your own queries from Part 4
// and either the crime data from test/data.ts or your own data. You can look
// at test/q.ts to see how to import and use the queries and data.
// The outline of the first test has been provided as an example - you should
// modify it so it actually does something interesting.

describe("custom", () => {

    it("optimizes countIf with multiple chained filters correctly", () => {
        expectQuerySequence(queries.numberOfThreatsOnABlockQuery, ["Apply", "CountIf"]);
    });

    it("runs powerQuery correctly", () => {
        const data = [1, 2, 3];
        const expected = [
            {"base": 1, "power 2": 1, "power 3": 1},
            {"base": 2, "power 2": 4, "power 3": 8},
            {"base": 3, "power 2": 9, "power 3": 27}
        ];
        const out = queries.powerQuery.execute(data);
        expect(out).to.deep.equal(expected)
    });

    it("runs comboMealQuery correctly", () => {
        const menu = [
            {"name": "burger", "type": "food"},
            {"name": "pizza", "type": "food"},
            {"name": "milkshake", "type": "drink"},
            {"name": "soda", "type": "drink"}
        ];
        const expected = [
            {
                "left": {"name": "burger", "type": "food"},
                "right": {"name": "milkshake", "type": "drink"}
            },
            {
                "left": {"name": "burger", "type": "food"},
                "right": {"name": "soda", "type": "drink"}
            },
            {
                "left": {"name": "pizza", "type": "food"},
                "right": {"name": "milkshake", "type": "drink"}
            },
            {
                "left": {"name": "pizza", "type": "food"},
                "right": {"name": "soda", "type": "drink"}
            }
        ];
        const out = queries.comboMealQuery.execute(menu);
        expect(out).to.deep.equal(expected)
    });

    it("runs theftsOnABlockQuery correctly", () => {
        const expected = [];
        const data = queries.cleanupQuery2.execute(rawData);
        for (const datum of data) {
            if ((datum.type as string).match(/THEFT/)
                    && (datum.area as string).match(/BLOCK/)) {
                expected.push(datum);
            }
        }
        const out = queries.theftsOnABlockQuery.execute(rawData);
        expect(out).to.deep.equal(expected);
    });

    it("runs numberOfThreatsOnABlockQuery correctly", () => {
        var count = 0;
        const data = queries.cleanupQuery2.execute(rawData);
        for (const datum of data) {
            if ((datum.type as string).match(/THREAT/)
                    && (datum.area as string).match(/BLOCK/)) {
                count++;
            }
        }
        const out = queries.numberOfThreatsOnABlockQuery.execute(rawData);
        expect(out[0]).to.equal(count);
    });
});
