// copy-pasta from https://data.seattle.gov/api/views/vtr2-wp3r/rows.json as of 1/12/2013
export const crimeData = {
  "meta" : {
    "view" : {
      "id" : "vtr2-wp3r",
      "name" : "Seattle Major Crimes Last 48 Hours",
      "averageRating" : 0,
      "category" : "Public Safety",
      "createdAt" : 1425913343,
      "displayType" : "table",
      "downloadCount" : 1205,
      "hideFromCatalog" : false,
      "hideFromDataJson" : false,
      "indexUpdatedAt" : 1474996689,
      "licenseId" : "PUBLIC_DOMAIN",
      "newBackend" : false,
      "numberOfComments" : 0,
      "oid" : 21745684,
      "provenance" : "official",
      "publicationAppendEnabled" : true,
      "publicationDate" : 1466196985,
      "publicationGroup" : 2447069,
      "publicationStage" : "published",
      "rowClass" : "",
      "rowsUpdatedAt" : 1466196845,
      "rowsUpdatedBy" : "pfbu-yuv5",
      "tableId" : 11390067,
      "totalTimesRated" : 0,
      "viewCount" : 333,
      "viewLastModified" : 1474996575,
      "viewType" : "tabular",
      "columns" : [ {
        "id" : -1,
        "name" : "sid",
        "dataTypeName" : "meta_data",
        "fieldName" : ":sid",
        "position" : 0,
        "renderTypeName" : "meta_data",
        "format" : { },
        "flags" : [ "hidden" ]
      }, {
        "id" : -1,
        "name" : "id",
        "dataTypeName" : "meta_data",
        "fieldName" : ":id",
        "position" : 0,
        "renderTypeName" : "meta_data",
        "format" : { },
        "flags" : [ "hidden" ]
      }, {
        "id" : -1,
        "name" : "position",
        "dataTypeName" : "meta_data",
        "fieldName" : ":position",
        "position" : 0,
        "renderTypeName" : "meta_data",
        "format" : { },
        "flags" : [ "hidden" ]
      }, {
        "id" : -1,
        "name" : "created_at",
        "dataTypeName" : "meta_data",
        "fieldName" : ":created_at",
        "position" : 0,
        "renderTypeName" : "meta_data",
        "format" : { },
        "flags" : [ "hidden" ]
      }, {
        "id" : -1,
        "name" : "created_meta",
        "dataTypeName" : "meta_data",
        "fieldName" : ":created_meta",
        "position" : 0,
        "renderTypeName" : "meta_data",
        "format" : { },
        "flags" : [ "hidden" ]
      }, {
        "id" : -1,
        "name" : "updated_at",
        "dataTypeName" : "meta_data",
        "fieldName" : ":updated_at",
        "position" : 0,
        "renderTypeName" : "meta_data",
        "format" : { },
        "flags" : [ "hidden" ]
      }, {
        "id" : -1,
        "name" : "updated_meta",
        "dataTypeName" : "meta_data",
        "fieldName" : ":updated_meta",
        "position" : 0,
        "renderTypeName" : "meta_data",
        "format" : { },
        "flags" : [ "hidden" ]
      }, {
        "id" : -1,
        "name" : "meta",
        "dataTypeName" : "meta_data",
        "fieldName" : ":meta",
        "position" : 0,
        "renderTypeName" : "meta_data",
        "format" : { },
        "flags" : [ "hidden" ]
      }, {
        "id" : 259506858,
        "name" : "cosid",
        "dataTypeName" : "number",
        "fieldName" : "cosid",
        "position" : 1,
        "renderTypeName" : "number",
        "tableColumnId" : 26424057,
        "width" : 148,
        "cachedContents" : {
          "non_null" : 44,
          "average" : "94353.45454545455",
          "largest" : "95440",
          "null" : 0,
          "top" : [ {
            "item" : "95020",
            "count" : 20
          }, {
            "item" : "94999",
            "count" : 19
          }, {
            "item" : "94925",
            "count" : 18
          } ],
          "smallest" : "92182",
          "sum" : "4151552"
        },
        "format" : {
          "precisionStyle" : "standard",
          "noCommas" : "false",
          "align" : "right"
        }
      }, {
        "id" : 259506859,
        "name" : "rms_cdw_id",
        "dataTypeName" : "text",
        "fieldName" : "rms_cdw_id",
        "position" : 2,
        "renderTypeName" : "text",
        "tableColumnId" : 26424058,
        "width" : 220,
        "cachedContents" : {
          "non_null" : 44,
          "largest" : "771797",
          "null" : 0,
          "top" : [ {
            "item" : "771395",
            "count" : 20
          }, {
            "item" : "771374",
            "count" : 19
          }, {
            "item" : "771296",
            "count" : 18
          } ],
          "smallest" : "768540"
        },
        "format" : { }
      }, {
        "id" : 259506860,
        "name" : "general_offense_number",
        "dataTypeName" : "text",
        "fieldName" : "general_offense_number",
        "position" : 3,
        "renderTypeName" : "text",
        "tableColumnId" : 26424059,
        "width" : 364,
        "cachedContents" : {
          "non_null" : 44,
          "largest" : "2016108075",
          "null" : 0,
          "top" : [ {
            "item" : "2016106780",
            "count" : 20
          }, {
            "item" : "2016106641",
            "count" : 19
          }, {
            "item" : "2016106637",
            "count" : 18
          } ],
          "smallest" : "2016106468"
        },
        "format" : { }
      }, {
        "id" : 259506861,
        "name" : "offense_code",
        "dataTypeName" : "text",
        "fieldName" : "offense_code",
        "position" : 4,
        "renderTypeName" : "text",
        "tableColumnId" : 26424060,
        "width" : 244,
        "cachedContents" : {
          "non_null" : 44,
          "largest" : "2699",
          "null" : 0,
          "top" : [ {
            "item" : "1305",
            "count" : 20
          }, {
            "item" : "2606",
            "count" : 19
          }, {
            "item" : "1316",
            "count" : 18
          }, {
            "item" : "2404",
            "count" : 17
          }, {
            "item" : "2589",
            "count" : 16
          }, {
            "item" : "2605",
            "count" : 15
          }, {
            "item" : "1204",
            "count" : 14
          }, {
            "item" : "2303",
            "count" : 13
          }, {
            "item" : "1206",
            "count" : 12
          }, {
            "item" : "2304",
            "count" : 11
          }, {
            "item" : "1313",
            "count" : 10
          }, {
            "item" : "2202",
            "count" : 9
          }, {
            "item" : "2301",
            "count" : 8
          }, {
            "item" : "2599",
            "count" : 7
          }, {
            "item" : "2399",
            "count" : 6
          }, {
            "item" : "2699",
            "count" : 5
          } ],
          "smallest" : "1204"
        },
        "format" : { }
      }, {
        "id" : 259506862,
        "name" : "offense_code_extension",
        "dataTypeName" : "text",
        "fieldName" : "offense_code_extension",
        "position" : 5,
        "renderTypeName" : "text",
        "tableColumnId" : 26424061,
        "width" : 364,
        "cachedContents" : {
          "non_null" : 44,
          "largest" : "8",
          "null" : 0,
          "top" : [ {
            "item" : "0",
            "count" : 20
          }, {
            "item" : "1",
            "count" : 19
          }, {
            "item" : "4",
            "count" : 18
          }, {
            "item" : "5",
            "count" : 17
          }, {
            "item" : "2",
            "count" : 16
          }, {
            "item" : "8",
            "count" : 15
          }, {
            "item" : "3",
            "count" : 14
          }, {
            "item" : "7",
            "count" : 13
          } ],
          "smallest" : "0"
        },
        "format" : { }
      }, {
        "id" : 259506863,
        "name" : "offense_type",
        "dataTypeName" : "text",
        "fieldName" : "offense_type",
        "position" : 6,
        "renderTypeName" : "text",
        "tableColumnId" : 26424062,
        "width" : 244,
        "cachedContents" : {
          "non_null" : 44,
          "largest" : "VEH-THEFT-TRUCK",
          "null" : 0,
          "top" : [ {
            "item" : "ASSLT-AGG-WEAPON",
            "count" : 20
          }, {
            "item" : "FRAUD-CHECK",
            "count" : 19
          }, {
            "item" : "THREATS-OTHER",
            "count" : 18
          }, {
            "item" : "VEH-THEFT-AUTO",
            "count" : 17
          }, {
            "item" : "THREATS-WEAPON",
            "count" : 16
          }, {
            "item" : "FORGERY-OTH",
            "count" : 15
          }, {
            "item" : "FRAUD-CREDIT CARD",
            "count" : 14
          }, {
            "item" : "ROBBERY-STREET-GUN",
            "count" : 13
          }, {
            "item" : "THEFT-SHOPLIFT",
            "count" : 12
          }, {
            "item" : "ROBBERY-STREET-BODYFORCE",
            "count" : 11
          }, {
            "item" : "THEFT-LICENSE PLATE",
            "count" : 10
          }, {
            "item" : "ASSLT-NONAGG",
            "count" : 9
          }, {
            "item" : "VEH-THEFT-TRUCK",
            "count" : 8
          }, {
            "item" : "BURGLARY-FORCE-RES",
            "count" : 7
          }, {
            "item" : "THEFT-PKPOCKET",
            "count" : 6
          }, {
            "item" : "COUNTERFEIT",
            "count" : 5
          }, {
            "item" : "THEFT-OTH",
            "count" : 4
          }, {
            "item" : "VEH-THEFT-TRAILER",
            "count" : 3
          }, {
            "item" : "FRAUD-OTHER",
            "count" : 2
          } ],
          "smallest" : "ASSLT-AGG-WEAPON"
        },
        "format" : { }
      }, {
        "id" : 259506864,
        "name" : "summary_offense_code",
        "dataTypeName" : "text",
        "fieldName" : "summary_offense_code",
        "position" : 7,
        "renderTypeName" : "text",
        "tableColumnId" : 26424063,
        "width" : 340,
        "cachedContents" : {
          "non_null" : 44,
          "largest" : "2600",
          "null" : 0,
          "top" : [ {
            "item" : "1300",
            "count" : 20
          }, {
            "item" : "2600",
            "count" : 19
          }, {
            "item" : "2400",
            "count" : 18
          }, {
            "item" : "2500",
            "count" : 17
          }, {
            "item" : "1200",
            "count" : 16
          }, {
            "item" : "2300",
            "count" : 15
          }, {
            "item" : "2200",
            "count" : 14
          } ],
          "smallest" : "1200"
        },
        "format" : { }
      }, {
        "id" : 259506865,
        "name" : "summarized_offense_description",
        "dataTypeName" : "text",
        "fieldName" : "summarized_offense_description",
        "position" : 8,
        "renderTypeName" : "text",
        "tableColumnId" : 26424064,
        "width" : 460,
        "cachedContents" : {
          "non_null" : 44,
          "largest" : "VEHICLE THEFT",
          "null" : 0,
          "top" : [ {
            "item" : "ASSAULT",
            "count" : 20
          }, {
            "item" : "FRAUD",
            "count" : 19
          }, {
            "item" : "THREATS",
            "count" : 18
          }, {
            "item" : "VEHICLE THEFT",
            "count" : 17
          }, {
            "item" : "FORGERY",
            "count" : 16
          }, {
            "item" : "ROBBERY",
            "count" : 15
          }, {
            "item" : "SHOPLIFTING",
            "count" : 14
          }, {
            "item" : "OTHER PROPERTY",
            "count" : 13
          }, {
            "item" : "BURGLARY",
            "count" : 12
          }, {
            "item" : "PICKPOCKET",
            "count" : 11
          }, {
            "item" : "COUNTERFEIT",
            "count" : 10
          } ],
          "smallest" : "ASSAULT"
        },
        "format" : { }
      }, {
        "id" : 259506866,
        "name" : "date_reported",
        "dataTypeName" : "date",
        "fieldName" : "date_reported",
        "position" : 9,
        "renderTypeName" : "date",
        "tableColumnId" : 26424065,
        "width" : 256,
        "cachedContents" : {
          "non_null" : 44,
          "largest" : 1459251660,
          "null" : 0,
          "top" : [ {
            "item" : 1459164900,
            "count" : 20
          }, {
            "item" : 1459157040,
            "count" : 19
          }, {
            "item" : 1459156080,
            "count" : 18
          } ],
          "smallest" : 1459143420
        },
        "format" : { }
      }, {
        "id" : 259506867,
        "name" : "occurred_date_or_date_range_start",
        "dataTypeName" : "date",
        "fieldName" : "occurred_date_or_date_range_start",
        "position" : 10,
        "renderTypeName" : "date",
        "tableColumnId" : 26424066,
        "width" : 496,
        "cachedContents" : {
          "non_null" : 44,
          "largest" : 1459221720,
          "null" : 0,
          "top" : [ {
            "item" : 1459092600,
            "count" : 20
          }, {
            "item" : 1458936000,
            "count" : 19
          }, {
            "item" : 1459031400,
            "count" : 18
          } ],
          "smallest" : 1451405700
        },
        "format" : { }
      }, {
        "id" : 259506868,
        "name" : "occurred_date_range_end",
        "dataTypeName" : "date",
        "fieldName" : "occurred_date_range_end",
        "position" : 11,
        "renderTypeName" : "date",
        "tableColumnId" : 26424067,
        "width" : 376,
        "cachedContents" : {
          "non_null" : 24,
          "largest" : 1459222200,
          "null" : 20,
          "top" : [ {
            "item" : 1459166400,
            "count" : 20
          }, {
            "item" : 1459188000,
            "count" : 19
          }, {
            "item" : 1458831600,
            "count" : 18
          }, {
            "item" : 1459116000,
            "count" : 17
          }, {
            "item" : 1458748800,
            "count" : 16
          }, {
            "item" : 1458928800,
            "count" : 15
          }, {
            "item" : 1458493200,
            "count" : 14
          }, {
            "item" : 1459187400,
            "count" : 13
          }, {
            "item" : 1459162800,
            "count" : 12
          }, {
            "item" : 1459152000,
            "count" : 11
          }, {
            "item" : 1459220400,
            "count" : 10
          }, {
            "item" : 1459194420,
            "count" : 9
          }, {
            "item" : 1459171800,
            "count" : 8
          }, {
            "item" : 1459184400,
            "count" : 7
          }, {
            "item" : 1459126800,
            "count" : 6
          }, {
            "item" : 1458970200,
            "count" : 5
          }, {
            "item" : 1459152600,
            "count" : 4
          }, {
            "item" : 1459175400,
            "count" : 3
          }, {
            "item" : 1459222200,
            "count" : 2
          }, {
            "item" : 1459211520,
            "count" : 1
          } ],
          "smallest" : 1458493200
        },
        "format" : { }
      }, {
        "id" : 259506869,
        "name" : "hundred_block_location",
        "dataTypeName" : "text",
        "fieldName" : "hundred_block_location",
        "position" : 12,
        "renderTypeName" : "text",
        "tableColumnId" : 26424068,
        "width" : 364,
        "cachedContents" : {
          "non_null" : 44,
          "largest" : "NE 145 ST / LAKE CITY WY NE",
          "null" : 0,
          "top" : [ {
            "item" : "27XX BLOCK OF AIRPORT WY S",
            "count" : 20
          }, {
            "item" : "4XX BLOCK OF 26 AV E",
            "count" : 19
          }, {
            "item" : "19XX BLOCK OF 2 AV",
            "count" : 18
          } ],
          "smallest" : "102XX BLOCK OF LAKE CITY WY NE"
        },
        "format" : { }
      }, {
        "id" : 259506870,
        "name" : "district_sector",
        "dataTypeName" : "text",
        "fieldName" : "district_sector",
        "position" : 13,
        "renderTypeName" : "text",
        "tableColumnId" : 26424069,
        "width" : 280,
        "cachedContents" : {
          "non_null" : 44,
          "largest" : "U",
          "null" : 0,
          "top" : [ {
            "item" : "K",
            "count" : 20
          }, {
            "item" : "L",
            "count" : 19
          }, {
            "item" : "D",
            "count" : 18
          }, {
            "item" : "B",
            "count" : 17
          }, {
            "item" : "S",
            "count" : 16
          }, {
            "item" : "U",
            "count" : 15
          }, {
            "item" : "M",
            "count" : 14
          }, {
            "item" : "E",
            "count" : 13
          }, {
            "item" : "G",
            "count" : 12
          }, {
            "item" : "C",
            "count" : 11
          }, {
            "item" : "F",
            "count" : 10
          }, {
            "item" : "N",
            "count" : 9
          }, {
            "item" : "R",
            "count" : 8
          }, {
            "item" : "J",
            "count" : 7
          }, {
            "item" : "Q",
            "count" : 6
          }, {
            "item" : "O",
            "count" : 5
          } ],
          "smallest" : "B"
        },
        "format" : { }
      }, {
        "id" : 259506871,
        "name" : "zone_beat",
        "dataTypeName" : "text",
        "fieldName" : "zone_beat",
        "position" : 14,
        "renderTypeName" : "text",
        "tableColumnId" : 26424070,
        "width" : 208,
        "cachedContents" : {
          "non_null" : 44,
          "largest" : "U3",
          "null" : 0,
          "top" : [ {
            "item" : "K1",
            "count" : 20
          }, {
            "item" : "L2",
            "count" : 19
          }, {
            "item" : "D3",
            "count" : 18
          }, {
            "item" : "B2",
            "count" : 17
          }, {
            "item" : "B1",
            "count" : 16
          }, {
            "item" : "S3",
            "count" : 15
          }, {
            "item" : "U3",
            "count" : 14
          }, {
            "item" : "K3",
            "count" : 13
          }, {
            "item" : "M2",
            "count" : 12
          }, {
            "item" : "E2",
            "count" : 11
          }, {
            "item" : "G1",
            "count" : 10
          }, {
            "item" : "C2",
            "count" : 9
          }, {
            "item" : "F2",
            "count" : 8
          }, {
            "item" : "N2",
            "count" : 7
          }, {
            "item" : "S1",
            "count" : 6
          }, {
            "item" : "L1",
            "count" : 5
          }, {
            "item" : "R1",
            "count" : 4
          }, {
            "item" : "J2",
            "count" : 3
          }, {
            "item" : "F1",
            "count" : 2
          }, {
            "item" : "U2",
            "count" : 1
          } ],
          "smallest" : "B1"
        },
        "format" : { }
      }, {
        "id" : 259506872,
        "name" : "census_tract_2000",
        "dataTypeName" : "text",
        "fieldName" : "census_tract_2000",
        "position" : 15,
        "renderTypeName" : "text",
        "tableColumnId" : 26424071,
        "width" : 304,
        "cachedContents" : {
          "non_null" : 44,
          "largest" : "9400.5015",
          "null" : 0,
          "top" : [ {
            "item" : "9300.3083",
            "count" : 20
          }, {
            "item" : "7700.2008",
            "count" : 19
          }, {
            "item" : "8002.1002",
            "count" : 18
          } ],
          "smallest" : "100.4000"
        },
        "format" : { }
      }, {
        "id" : 259506873,
        "name" : "longitude",
        "dataTypeName" : "number",
        "fieldName" : "longitude",
        "position" : 16,
        "renderTypeName" : "number",
        "tableColumnId" : 26424072,
        "width" : 208,
        "cachedContents" : {
          "non_null" : 44,
          "average" : "-122.3294221704318",
          "largest" : "-122.26449585",
          "null" : 0,
          "top" : [ {
            "item" : "-122.321395874",
            "count" : 20
          }, {
            "item" : "-122.298774719",
            "count" : 19
          }, {
            "item" : "-122.3411026",
            "count" : 18
          } ],
          "smallest" : "-122.399749756",
          "sum" : "-5382.494575499"
        },
        "format" : { }
      }, {
        "id" : 259506874,
        "name" : "latitude",
        "dataTypeName" : "number",
        "fieldName" : "latitude",
        "position" : 17,
        "renderTypeName" : "number",
        "tableColumnId" : 26424073,
        "width" : 196,
        "cachedContents" : {
          "non_null" : 44,
          "average" : "47.61792443006818",
          "largest" : "47.733753204",
          "null" : 0,
          "top" : [ {
            "item" : "47.579654694",
            "count" : 20
          }, {
            "item" : "47.623126984",
            "count" : 19
          }, {
            "item" : "47.611347198",
            "count" : 18
          } ],
          "smallest" : "47.517791748",
          "sum" : "2095.188674923"
        },
        "format" : { }
      }, {
        "id" : 259506875,
        "name" : "Relative_Date",
        "dataTypeName" : "date",
        "fieldName" : "relative_date",
        "position" : 18,
        "renderTypeName" : "date",
        "tableColumnId" : 26424074,
        "width" : 256,
        "cachedContents" : {
          "non_null" : 44,
          "largest" : 1459141254,
          "null" : 0,
          "top" : [ {
            "item" : 1459141254,
            "count" : 20
          }, {
            "item" : 1459141218,
            "count" : 19
          } ],
          "smallest" : 1459141218
        },
        "format" : { }
      }, {
        "id" : 259506876,
        "name" : "geom",
        "dataTypeName" : "location",
        "fieldName" : "geom",
        "position" : 19,
        "renderTypeName" : "location",
        "tableColumnId" : 26424075,
        "width" : 148,
        "cachedContents" : {
          "non_null" : 44,
          "largest" : {
            "latitude" : "47.601722717",
            "human_address" : "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}",
            "longitude" : "-122.334182739"
          },
          "null" : 0,
          "top" : [ {
            "item" : {
              "latitude" : "47.579654694",
              "human_address" : "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}",
              "longitude" : "-122.321395874"
            },
            "count" : 20
          }, {
            "item" : {
              "latitude" : "47.623126984",
              "human_address" : "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}",
              "longitude" : "-122.298774719"
            },
            "count" : 19
          }, {
            "item" : {
              "latitude" : "47.611347198",
              "human_address" : "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}",
              "longitude" : "-122.3411026"
            },
            "count" : 18
          } ],
          "smallest" : {
            "latitude" : "47.601722717",
            "human_address" : "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}",
            "longitude" : "-122.334182739"
          }
        },
        "format" : { },
        "subColumnTypes" : [ "human_address", "latitude", "longitude", "machine_address", "needs_recoding" ]
      } ],
      "grants" : [ {
        "inherited" : false,
        "type" : "viewer",
        "flags" : [ "public" ]
      } ],
      "license" : {
        "name" : "Public Domain"
      },
      "metadata" : {
        "jsonQuery" : {
          "order" : [ {
            "ascending" : true,
            "columnFieldName" : "__id"
          } ]
        },
        "rdfClass" : "",
        "rdfSubject" : "0",
        "custom_fields" : {
          "Refresh Frequency" : {
            "Frequency" : "0"
          },
          "Data Owner" : {
            "Owner" : "Seattle Police"
          }
        },
        "rowIdentifier" : "0",
        "availableDisplayTypes" : [ "table", "fatrow", "page" ],
        "rowLabel" : "Row",
        "renderTypeConfig" : {
          "visible" : {
            "table" : true
          }
        }
      },
      "owner" : {
        "id" : "pfbu-yuv5",
        "displayName" : "Seattle IT",
        "profileImageUrlLarge" : "/api/users/pfbu-yuv5/profile_images/LARGE",
        "profileImageUrlMedium" : "/api/users/pfbu-yuv5/profile_images/THUMB",
        "profileImageUrlSmall" : "/api/users/pfbu-yuv5/profile_images/TINY",
        "screenName" : "Seattle IT"
      },
      "query" : {
        "orderBys" : [ {
          "ascending" : true,
          "expression" : {
            "columnId" : 259506858,
            "type" : "column"
          }
        } ]
      },
      "rights" : [ "read" ],
      "tableAuthor" : {
        "id" : "pfbu-yuv5",
        "displayName" : "Seattle IT",
        "profileImageUrlLarge" : "/api/users/pfbu-yuv5/profile_images/LARGE",
        "profileImageUrlMedium" : "/api/users/pfbu-yuv5/profile_images/THUMB",
        "profileImageUrlSmall" : "/api/users/pfbu-yuv5/profile_images/TINY",
        "screenName" : "Seattle IT"
      },
      "flags" : [ "default", "restorable", "restorePossibleForType" ]
    }
  },
  "data" : [ [ 25965, "0A92F99F-C5AF-4C9A-95E7-47DF6E43347A", 25965, 1459314183, "386118", 1459314183, "386118", null, "95018", "771393", "2016107414", "1305", "0", "ASSLT-AGG-WEAPON", "1300", "ASSAULT", 1459198980, 1459198980, null, "1 AV / YESLER WY", "K", "K1", "9200.2007", "-122.334182739", "47.601722717", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.601722717", "-122.334182739", null, false ] ]
, [ 25966, "4366EB34-3F80-49EE-B606-C7A67DF9D3BA", 25966, 1459314183, "386118", 1459314183, "386118", null, "94928", "771299", "2016107189", "2606", "1", "FRAUD-CHECK", "2600", "FRAUD", 1459184640, 1456790400, 1459166400, "102XX BLOCK OF LAKE CITY WY NE", "L", "L2", "1100.2015", "-122.301612854", "47.703125", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.703125", "-122.301612854", null, false ] ]
, [ 25967, "B6830D9F-47F0-4F31-B815-B2F55617D256", 25967, 1459314183, "386118", 1459314183, "386118", null, "95019", "771394", "2016106667", "1316", "4", "THREATS-OTHER", "1300", "THREATS", 1459158120, 1459158120, null, "3XX BLOCK OF PONTIUS AV N", "D", "D3", "7300.1027", "-122.331710815", "47.621391296", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.621391296", "-122.331710815", null, false ] ]
, [ 25968, "6CCB538C-710B-4758-8403-376CE658B7F7", 25968, 1459314183, "386118", 1459314183, "386118", null, "93836", "770202", "2016107313", "2404", "1", "VEH-THEFT-AUTO", "2400", "VEHICLE THEFT", 1459191540, 1459080000, 1459188000, "8XX BLOCK OF N 36 ST", "B", "B2", "4900.2017", "-122.348930359", "47.651199341", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.651199341", "-122.348930359", null, false ] ]
, [ 25969, "9759F4E8-9DF4-4372-A5C2-172740FE8E94", 25969, 1459314183, "386118", 1459314183, "386118", null, "94826", "771195", "2016107662", "1316", "5", "THREATS-WEAPON", "1300", "THREATS", 1459220820, 1459220820, null, "14XX BLOCK OF NW MARKET ST", "B", "B1", "4700.3021", "-122.374900818", "47.66866684", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.66866684", "-122.374900818", null, false ] ]
, [ 25970, "BDEE3BBB-9E2D-472A-B39D-A8B8B1C61E40", 25970, 1459314183, "386118", 1459314183, "386118", null, "94517", "770884", "2016106901", "2589", "1", "FORGERY-OTH", "2500", "FORGERY", 1459171740, 1451405700, 1458831600, "90XX BLOCK OF SEWARD PARK AV S", "S", "S3", "11800.6018", "-122.26449585", "47.522407532", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.522407532", "-122.26449585", null, false ] ]
, [ 25971, "507B5AA6-C371-4C3A-9189-7D4327DF2D36", 25971, 1459314183, "386118", 1459314183, "386118", null, "95015", "771390", "2016107121", "2605", "0", "FRAUD-CREDIT CARD", "2600", "FRAUD", 1459185540, 1458900000, 1459116000, "26XX BLOCK OF NE 65 ST", "U", "U3", "4301.2002", "-122.298995972", "47.675777435", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.675777435", "-122.298995972", null, false ] ]
, [ 25972, "447C04F1-08A0-424B-BC50-F72E8B97297B", 25972, 1459314183, "386118", 1459314183, "386118", null, "94927", "771298", "2016107235", "1204", "0", "ROBBERY-STREET-GUN", "1200", "ROBBERY", 1459191240, 1458909900, null, "4 AV S / S WASHINGTON ST", "K", "K3", "9200.1004", "-122.328964233", "47.60087204", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.60087204", "-122.328964233", null, false ] ]
, [ 25973, "F45F4DE8-F658-4A6E-A462-C6FCAAF41210", 25973, 1459314183, "386118", 1459314183, "386118", null, "94819", "771188", "2016106993", "2303", "0", "THEFT-SHOPLIFT", "2300", "SHOPLIFTING", 1459176000, 1459175100, null, "6XX BLOCK OF PINE ST", "M", "M2", "8200.1002", "-122.334815979", "47.612369537", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.612369537", "-122.334815979", null, false ] ]
, [ 25974, "FD76A231-7226-4E4D-8C38-B2F2E60B023E", 25974, 1459314183, "386118", 1459314183, "386118", null, "95306", "771795", "2016107664", "1206", "0", "ROBBERY-STREET-BODYFORCE", "1200", "ROBBERY", 1459232520, 1459221720, null, "E PIKE ST / BOYLSTON AV", "E", "E2", "8400.1005", "-122.323402405", "47.614067078", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.614067078", "-122.323402405", null, false ] ]
, [ 25975, "CA426898-3981-4435-9FE4-EA0B40D4C131", 25975, 1459314183, "386118", 1459314183, "386118", null, "92766", "769128", "2016106596", "1316", "4", "THREATS-OTHER", "1300", "THREATS", 1459153740, 1453276800, 1458748800, "92XX BLOCK OF RAINIER AV S", "S", "S3", "11800.4010", "-122.269256592", "47.519920349", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.519920349", "-122.269256592", null, false ] ]
, [ 25976, "A841D322-061B-47B0-A3BA-874A758AC333", 25976, 1459314183, "386118", 1459314183, "386118", null, "93090", "769745", "2016106970", "2304", "2", "THEFT-LICENSE PLATE", "2300", "OTHER PROPERTY", 1459174440, 1458806400, 1458928800, "3XX BLOCK OF 9 AV", "G", "G1", "8500.3001", "-122.323440552", "47.604129791", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.604129791", "-122.323440552", null, false ] ]
, [ 25977, "41314C42-17B1-4FDF-B43C-6355A2220FD8", 25977, 1459314183, "386118", 1459314183, "386118", null, "95218", "771707", "2016106914", "1316", "4", "THREATS-OTHER", "1300", "THREATS", 1459171800, 1459015200, null, "19XX BLOCK OF E MADISON ST", "C", "C2", "7900.5001", "-122.306770325", "47.616996765", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.616996765", "-122.306770325", null, false ] ]
, [ 25978, "51B5D143-5800-4D7B-B0F1-082910DB8DDE", 25978, 1459314183, "386118", 1459314183, "386118", null, "94933", "771304", "2016107222", "1206", "0", "ROBBERY-STREET-BODYFORCE", "1200", "ROBBERY", 1459187280, 1459187280, null, "4XX BLOCK OF NE NORTHGATE WY", "L", "L2", "1200.4010", "-122.324615479", "47.708602905", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.708602905", "-122.324615479", null, false ] ]
, [ 25979, "23CEA02B-B1DC-451D-9C8B-B48845BC4A46", 25979, 1459314183, "386118", 1459314183, "386118", null, "94981", "771356", "2016106945", "1313", "0", "ASSLT-NONAGG", "1300", "ASSAULT", 1459177260, 1459173000, null, "24XX BLOCK OF SW HOLDEN ST", "F", "F2", "11401.1002", "-122.364501953", "47.533699036", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.533699036", "-122.364501953", null, false ] ]
, [ 25980, "D747F805-565A-4A84-BC54-356DB3E76E6C", 25980, 1459314183, "386118", 1459314183, "386118", null, "95297", "771786", "2016108075", "2404", "8", "VEH-THEFT-TRUCK", "2400", "VEHICLE THEFT", 1459251660, 1458032400, 1458493200, "127XX BLOCK OF AURORA AV N", "N", "N2", "402.2003", "-122.344963074", "47.721847534", 1459141218, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.721847534", "-122.344963074", null, false ] ]
, [ 25981, "C73753E1-1CD7-48E9-8792-28DEE139BEE0", 25981, 1459314183, "386118", 1459314183, "386118", null, "94619", "770987", "2016107245", "2202", "0", "BURGLARY-FORCE-RES", "2200", "BURGLARY", 1459188600, 1459185300, 1459187400, "31XX BLOCK OF S FRONTENAC ST", "S", "S1", "11002.2035", "-122.290794373", "47.53993988", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.53993988", "-122.290794373", null, false ] ]
, [ 25982, "284AAFB3-74E8-4FFD-AAD7-7FDDBCD530E7", 25982, 1459314183, "386118", 1459314183, "386118", null, "94924", "771293", "2016107640", "1204", "0", "ROBBERY-STREET-GUN", "1200", "ROBBERY", 1459218420, 1459218420, null, "NE 145 ST / LAKE CITY WY NE", "L", "L1", "100.4000", "-122.292434692", "47.733753204", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.733753204", "-122.292434692", null, false ] ]
, [ 25983, "7B062E81-8CB9-4179-8958-CA0241805AA7", 25983, 1459314183, "386118", 1459314183, "386118", null, "94332", "770664", "2016106785", "2202", "0", "BURGLARY-FORCE-RES", "2200", "BURGLARY", 1459167900, 1458984600, 1459162800, "38XX BLOCK OF 24 AV S", "R", "R1", "10001.1022", "-122.301811218", "47.569789886", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.569789886", "-122.301811218", null, false ] ]
, [ 25984, "485713E3-D5B5-404C-964A-23425969456B", 25984, 1459314183, "386118", 1459314183, "386118", null, "93095", "769750", "2016107029", "2304", "2", "THEFT-LICENSE PLATE", "2300", "OTHER PROPERTY", 1459179360, 1459179360, 1459152000, "73XX BLOCK OF 33 AV NW", "J", "J2", "3100.4015", "-122.399749756", "47.681999207", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.681999207", "-122.399749756", null, false ] ]
, [ 25985, "19F3EB88-FD69-4E11-A904-D5B76BF2DBB6", 25985, 1459314183, "386118", 1459314183, "386118", null, "95308", "771797", "2016107660", "2301", "0", "THEFT-PKPOCKET", "2300", "PICKPOCKET", 1459220460, 1459209600, 1459220400, "54XX BLOCK OF DELRIDGE WY SW", "F", "F1", "10701.1000", "-122.362976074", "47.552818298", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.552818298", "-122.362976074", null, false ] ]
, [ 25986, "8C1D9FED-7931-4612-B205-BC3267A6A8FB", 25986, 1459314183, "386118", 1459314183, "386118", null, "94334", "770666", "2016107349", "2304", "2", "THEFT-LICENSE PLATE", "2300", "OTHER PROPERTY", 1459212180, 1459189800, 1459194420, "12XX BLOCK OF N 143 ST", "N", "N2", "300.2015", "-122.343696594", "47.732311249", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.732311249", "-122.343696594", null, false ] ]
, [ 25987, "8A6FE107-DB1A-449F-B51F-5747BA0E7B38", 25987, 1459314183, "386118", 1459314183, "386118", null, "93722", "770088", "2016107050", "2404", "1", "VEH-THEFT-AUTO", "2400", "VEHICLE THEFT", 1459182420, 1458925200, 1459171800, "6XX BLOCK OF NW BRIGHT ST", "B", "B2", "4800.3003", "-122.364875793", "47.660015106", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.660015106", "-122.364875793", null, false ] ]
, [ 25988, "26FA8E9C-D8B9-471E-91E3-20B27B6CA5CF", 25988, 1459314183, "386118", 1459314183, "386118", null, "94621", "770989", "2016107507", "2303", "0", "THEFT-SHOPLIFT", "2300", "SHOPLIFTING", 1459204380, 1459202100, null, "6XX BLOCK OF 5 AV S", "K", "K3", "9100.2009", "-122.327682495", "47.596656799", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.596656799", "-122.327682495", null, false ] ]
, [ 25989, "A92ABFAE-1481-4B67-A127-98A035A4AB32", 25989, 1459314183, "386118", 1459314183, "386118", null, "93327", "769881", "2016106845", "2606", "1", "FRAUD-CHECK", "2600", "FRAUD", 1459173360, 1459168380, null, "5XX BLOCK OF S JACKSON ST", "K", "K3", "9100.2002", "-122.327011108", "47.599193573", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.599193573", "-122.327011108", null, false ] ]
, [ 25990, "41C0BB12-7480-4ECC-A3DE-54EC4E4F3CC6", 25990, 1459314183, "386118", 1459314183, "386118", null, "93825", "770191", "2016107266", "2404", "1", "VEH-THEFT-AUTO", "2400", "VEHICLE THEFT", 1459189080, 1459092600, 1459184400, "15XX BLOCK OF 17 AV S", "R", "R1", "9400.5015", "-122.310546875", "47.58921051", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.58921051", "-122.310546875", null, false ] ]
, [ 25991, "E111D681-4446-44B8-9C2F-2DFAECA578DC", 25991, 1459314183, "386118", 1459314183, "386118", null, "95440", "771702", "2016107171", "2599", "0", "COUNTERFEIT", "2500", "COUNTERFEIT", 1459184160, 1459184160, null, "45XX BLOCK OF UNIVERSITY WY NE", "U", "U2", "5301.2010", "-122.313117981", "47.662181854", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.662181854", "-122.313117981", null, false ] ]
, [ 25992, "0C23A151-C578-4300-89C3-0DF9D204C3BB", 25992, 1459314183, "386118", 1459314183, "386118", null, "94518", "770886", "2016107641", "2399", "3", "THEFT-OTH", "2300", "OTHER PROPERTY", 1459218600, 1459218600, null, "20XX BLOCK OF WESTLAKE AV", "D", "D2", "7200.1060", "-122.337875366", "47.615886688", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.615886688", "-122.337875366", null, false ] ]
, [ 25993, "3970F86D-2BC4-45C9-ACE8-965C1F815A46", 25993, 1459314183, "386118", 1459314183, "386118", null, "92386", "768746", "2016106577", "2404", "1", "VEH-THEFT-AUTO", "2400", "VEHICLE THEFT", 1459152720, 1459108800, 1459126800, "73XX BLOCK OF 29 AV SW", "F", "F1", "10701.2020", "-122.369804382", "47.536453247", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.536453247", "-122.369804382", null, false ] ]
, [ 25994, "78C3FE43-178B-43CA-88DD-C0AF85D3DFD7", 25994, 1459314183, "386118", 1459314183, "386118", null, "93091", "769746", "2016107059", "2404", "7", "VEH-THEFT-TRAILER", "2400", "VEHICLE THEFT", 1459178280, 1458943200, 1458970200, "84XX BLOCK OF DALLAS AV S", "F", "F3", "11200.2056", "-122.316139221", "47.527751923", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.527751923", "-122.316139221", null, false ] ]
, [ 25995, "F27C6B58-355E-47B3-B9C8-87A7050186A7", 25995, 1459314183, "386118", 1459314183, "386118", null, "94333", "770665", "2016107302", "2404", "1", "VEH-THEFT-AUTO", "2400", "VEHICLE THEFT", 1459190940, 1459011600, 1459152600, "15 AV / E OLIVE ST", "C", "C1", "7900.5006", "-122.312759399", "47.616447449", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.616447449", "-122.312759399", null, false ] ]
, [ 25996, "A508D39C-E670-4911-9AAA-C98D5CCAAFDB", 25996, 1459314183, "386118", 1459314183, "386118", null, "92182", "768540", "2016106468", "2404", "1", "VEH-THEFT-AUTO", "2400", "VEHICLE THEFT", 1459143420, 1459143300, null, "25XX BLOCK OF 28 AV W", "Q", "Q1", "5700.3007", "-122.392959595", "47.642753601", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.642753601", "-122.392959595", null, false ] ]
, [ 25997, "A689D93E-A9EB-4337-B010-D405CF0C84A2", 25997, 1459314183, "386118", 1459314183, "386118", null, "93089", "769744", "2016107081", "2404", "1", "VEH-THEFT-AUTO", "2400", "VEHICLE THEFT", 1459179420, 1459158300, 1459175400, "E OLIVE WY / HARVARD AV E", "E", "E1", "7402.1001", "-122.322212219", "47.619895935", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.619895935", "-122.322212219", null, false ] ]
, [ 25998, "162DEC3B-FC11-4227-8B8B-871C0F60A1A0", 25998, 1459314183, "386118", 1459314183, "386118", null, "94516", "770883", "2016107668", "2404", "1", "VEH-THEFT-AUTO", "2400", "VEHICLE THEFT", 1459222920, 1459195200, 1459222200, "84XX BLOCK OF 6 AV SW", "F", "F3", "11300.1021", "-122.342933655", "47.528270721", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.528270721", "-122.342933655", null, false ] ]
, [ 25999, "D8925E46-8E16-468D-8B36-543677D02473", 25999, 1459314183, "386118", 1459314183, "386118", null, "94513", "770879", "2016106827", "2699", "1", "FRAUD-OTHER", "2600", "FRAUD", 1459168680, 1458734400, null, "5XX BLOCK OF NE NORTHGATE WY", "L", "L2", "1200.3003", "-122.321914673", "47.708587646", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.708587646", "-122.321914673", null, false ] ]
, [ 26000, "7A67D97D-2647-4E84-B52A-BCF6AF1538D5", 26000, 1459314183, "386118", 1459314183, "386118", null, "94618", "770986", "2016107581", "2304", "2", "THEFT-LICENSE PLATE", "2300", "OTHER PROPERTY", 1459217040, 1458892800, 1459211520, "22XX BLOCK OF 2 AV", "D", "D1", "8001.2009", "-122.345001221", "47.61365509", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.61365509", "-122.345001221", null, false ] ]
, [ 26001, "38B39B33-54A1-4F10-BBF7-9B9599EC49C4", 26001, 1459314183, "386118", 1459314183, "386118", null, "92497", "768870", "2016106648", "2404", "1", "VEH-THEFT-AUTO", "2400", "VEHICLE THEFT", 1459156980, 1459092600, 1459148400, "32XX BLOCK OF SW MORGAN ST", "F", "F1", "10702.3001", "-122.374427795", "47.544670105", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.544670105", "-122.374427795", null, false ] ]
, [ 26002, "57774383-7905-404D-B927-BA3A32223D74", 26002, 1459314183, "386118", 1459314183, "386118", null, "93821", "770187", "2016107138", "2404", "1", "VEH-THEFT-AUTO", "2400", "VEHICLE THEFT", 1459193220, 1459185600, 1459193220, "94XX BLOCK OF 39 AV S", "S", "S1", "11700.2030", "-122.283668518", "47.517791748", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.517791748", "-122.283668518", null, false ] ]
, [ 26003, "CA01EBD4-E518-4917-AC3E-F48A52657271", 26003, 1459314183, "386118", 1459314183, "386118", null, "95195", "771571", "2016106963", "2202", "0", "BURGLARY-FORCE-RES", "2200", "BURGLARY", 1459177320, 1458975600, 1459166400, "3XX BLOCK OF W NICKERSON ST", "Q", "Q2", "5900.1029", "-122.362670898", "47.651233673", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.651233673", "-122.362670898", null, false ] ]
, [ 26004, "26D59A07-8350-4928-BBE5-E7B31CE935B0", 26004, 1459314183, "386118", 1459314183, "386118", null, "94620", "770988", "2016106748", "2202", "0", "BURGLARY-FORCE-RES", "2200", "BURGLARY", 1459163100, 1459120500, null, "29XX BLOCK OF NE BLAKELEY ST", "U", "U3", "4301.1006", "-122.296150208", "47.665557861", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.665557861", "-122.296150208", null, false ] ]
, [ 26005, "3AB1D505-3DD8-4F13-83DE-40E68D074A5D", 26005, 1459314183, "386118", 1459314183, "386118", null, "95186", "771562", "2016107638", "1313", "0", "ASSLT-NONAGG", "1300", "ASSAULT", 1459217940, 1459217940, null, "85XX BLOCK OF AURORA AV N", "N", "N3", "1800.2011", "-122.344528198", "47.690925598", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.690925598", "-122.344528198", null, false ] ]
, [ 26006, "F5D56D97-AA34-48DE-A0A3-1CD618C41589", 26006, 1459314183, "386118", 1459314183, "386118", null, "95020", "771395", "2016106780", "1316", "4", "THREATS-OTHER", "1300", "THREATS", 1459164900, 1459164900, null, "27XX BLOCK OF AIRPORT WY S", "O", "O2", "9300.3083", "-122.321395874", "47.579654694", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.579654694", "-122.321395874", null, false ] ]
, [ 26007, "9776781B-017D-4952-92FC-6994807F4ECA", 26007, 1459314183, "386118", 1459314183, "386118", null, "94999", "771374", "2016106641", "2605", "0", "FRAUD-CREDIT CARD", "2600", "FRAUD", 1459157040, 1458936000, null, "4XX BLOCK OF 26 AV E", "C", "C2", "7700.2008", "-122.298774719", "47.623126984", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.623126984", "-122.298774719", null, false ] ]
, [ 26008, "FDB64F2D-46F2-4E77-8475-B7E57815FA19", 26008, 1459314183, "386118", 1459314183, "386118", null, "94925", "771296", "2016106637", "2304", "2", "THEFT-LICENSE PLATE", "2300", "OTHER PROPERTY", 1459156080, 1459031400, 1459153800, "19XX BLOCK OF 2 AV", "M", "M1", "8002.1002", "-122.3411026", "47.611347198", 1459141254, [ "{\"address\":\"\",\"city\":\"\",\"state\":\"\",\"zip\":\"\"}", "47.611347198", "-122.3411026", null, false ] ]
 ]
};
