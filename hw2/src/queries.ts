import * as q from "./q";
const Q = q.Q;

//// 1.2 write a query

export const theftsQuery = new q.ThenNode(
    new q.IdNode(),
    new q.FilterNode((datum) => (datum[13] as string).match(/THEFT/) !== null)
);
export const autoTheftsQuery = new q.ThenNode(
    new q.IdNode(),
    new q.FilterNode((datum) => (datum[13] as string).match(/^VEH-THEFT/) !== null)
);

//// 1.4 clean the data

var cleanFn = function(datum) {
    return {
       "type": datum[13],
       "description": datum[15],
       "date": datum[17],
       "area": datum[19]
   };
};

export const cleanupQuery = new q.ThenNode(
    new q.IdNode(),
    new q.ApplyNode(cleanFn)
);

//// 1.6 reimplement queries with call-chaining

export const cleanupQuery2 = Q.apply(cleanFn);
export const theftsQuery2 = Q.filter((datum) => (datum.type as string).match(/THEFT/) !== null);
export const autoTheftsQuery2 = Q.filter((datum) => (datum.type as string).match(/^VEH-THEFT/) !== null);

//// 4 put your queries here (remember to export them for use in tests)

var getPow = function(num, pow) {
    var powObj = {"base": num};
    powObj[`power ${pow}`] = Math.pow(num, pow);
    return powObj;
}

// Given a list of numbers, calculates the 2nd and 3rd power of each number and
// joins both onto one row for easy reference
export const powerQuery = Q.apply((num) => getPow(num, 2))
        .join(Q.apply((num) => getPow(num, 3)), "base")
        .optimize();

// Given a menu containing a list of food items and drinks, returns all possible
// combinations of a "combo meal" (i.e. one food item and one drink)
export const comboMealQuery = Q.filter((item) => item.type === "food")
        .product(Q.filter((item) => item.type === "drink"));

// Given a list of crime data, cleans the data and then finds all the thefts that
// occurred on a block
export const theftsOnABlockQuery = Q.apply(cleanFn)
        .filter((datum) => (datum.type as string).match(/THEFT/) !== null)
        .filter((datum) => (datum.area as string).match(/BLOCK/) !== null)
        .optimize();

// Given a list of crime data, cleans the data and then finds the number of threats
// that occurred on a block
export const numberOfThreatsOnABlockQuery = Q.apply(cleanFn)
        .filter((datum) => (datum.type as string).match(/THREAT/) !== null)
        .filter((datum) => (datum.area as string).match(/BLOCK/) !== null)
        .count()
        .optimize();