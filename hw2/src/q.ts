/* Q is a small query language for JavaScript.
 *
 * Q supports simple queries over a JSON-style data structure,
 * kinda like the functional version of LINQ in C#.
 *
 */

// This class represents all AST Nodes.
export class ASTNode {
    public static combineFn: ((datum: any) => any) = function (datum: any) {
        return Object.assign({}, datum.left, datum.right);
    };

    public readonly type: string;

    constructor(type: string) {
        this.type = type;
    }

    execute(data: any[]): any {
        throw new Error("Execute not implemented for " + this.type + " node.");
    }

    optimize(): ASTNode {
        return this;
    }

    run(data: any[]): any {
        return this.optimize().execute(data);
    }

    //// 1.5 implement call-chaining

    filter(predicate: (datum: any) => boolean): ASTNode {
        return new ThenNode(this, new FilterNode(predicate));
    }

    apply(callback: (datum: any) => any): ASTNode {
        return new ThenNode(this, new ApplyNode(callback));
    }

    count(): ASTNode {
        return new ThenNode(this, new CountNode());
    }

    product(query: ASTNode): ASTNode {
        return new CartesianProductNode(this, query);
    }

    join(query: ASTNode, relation: string | ((left: any, right: any) => any)): ASTNode {
        var predicate;
        if (typeof(relation) === "string") {
            predicate = (datum) => datum.left[relation] == datum.right[relation];
            predicate.type = "field";
            predicate.field = relation;
        } else {
            predicate = (datum) => relation(datum.left, datum.right);
            predicate.type = "function";
        }
        return new CartesianProductNode(this, query)
                .filter(predicate)
                .apply(ASTNode.combineFn);
    }
}

// The Id node just outputs all records from input.
export class IdNode extends ASTNode {

    constructor() {
        super("Id");
    }

    execute(data: any[]): any {
        return data.slice();
    }
}

// We can use an Id node as a convenient starting point for
// the call-chaining interface.
export const Q = new IdNode();

// The Filter node uses a callback to throw out some records.
export class FilterNode extends ASTNode {
    predicate: (datum: any) => boolean;

    constructor(predicate: (datum: any) => boolean) {
        super("Filter");
        this.predicate = predicate;
    }

    execute(data: any[]): any {
        return data.filter(this.predicate);
    }
}

// The Then node chains multiple actions on one data structure.
export class ThenNode extends ASTNode {
    first: ASTNode;
    second: ASTNode;

    constructor(first: ASTNode, second: ASTNode) {
        super("Then");
        this.first = first;
        this.second = second;
    }

    //// 1.1 implement execute

    execute(data: any[]): any {
        return this.second.execute(
            this.first.execute(data)
        );
    }
}

//// 1.3 implement Apply and Count Nodes

export class ApplyNode extends ASTNode {
    callback: (datum: any) => any;

    constructor(callback: (datum: any) => any) {
        super("Apply");
        this.callback = callback;
    }

    execute(data: any[]): any {
        return data.map(this.callback);
    }
}

export class CountNode extends ASTNode {
    constructor() {
        super("Count");
    }

    execute(data: any[]): any {
        return [data.length];
    }
}

//// 2.1 optimize queries

// This function permanently adds a new optimization function to a node type.
// An optimization function takes in a node and either returns a new,
// optimized node or null if no optimizations can be performed. AddOptimization
// will register this function to a particular node type, so that it will be called
// as part of that node's optimize() method, along with all other registered
// optimizations.
function AddOptimization(nodeType, opt: (this: ASTNode) => ASTNode | null) {
    const oldOptimize = nodeType.prototype.optimize;
    nodeType.prototype.optimize = function(this: ASTNode): ASTNode {
        const newThis = oldOptimize.call(this);
        return opt.call(newThis) || newThis;
    };
}

// For example, the following small optimization removes unnecessary Id nodes.
AddOptimization(ThenNode, function(this: ThenNode): ASTNode {
    if (this.first instanceof IdNode) {
        return this.second;
    } else if (this.second instanceof IdNode) {
        return this.first;
    } else {
        return null;
    }
});

// The above optimization has a few notable side effects. First, it removes the
// root IdNode from your call chain, so if you were depending on that to exist
// your other optimizations may be confused. Also, it returns the interesting
// subtree directly, without trying to optimize it more. It is up to you to
// determine where additional optimization should occur.

// We won't specifically test for this optimization, so if it's giving you
// a headache feel free to comment it out.

// You can add optimizations for part 2.1 here (or anywhere below).
// ...

function optimizeFilters(node: ThenNode): ASTNode {
    while (node.first instanceof ThenNode
            && node.second instanceof FilterNode
            && node.first.second instanceof FilterNode) {
        const predicateA = (node.second as FilterNode).predicate;
        const predicateB = ((node.first as ThenNode).second as FilterNode).predicate;
        node.second = new FilterNode((datum) => predicateA(datum) && predicateB(datum));
        node.first = node.first.first;
    }
    return node;
}

AddOptimization(ThenNode, function(this: ThenNode): ASTNode {
    if (this.second instanceof FilterNode) {
        optimizeFilters(this);
    } else {
        return null;
    }
});

//// 2.2 internal node types and CountIf

export class CountIfNode extends ASTNode {
    predicate: (datum: any) => boolean;

    constructor(predicate: (datum: any) => boolean) {
        super("CountIf");
        this.predicate = predicate
    }

    execute(data: any[]): any {
        var count = 0;
        for (const datum of data) {
            if (this.predicate(datum)) {
                count++;
            }
        }
        return [count];
    }
}

AddOptimization(ThenNode, function(this: ThenNode): ASTNode {
    if (this.second instanceof CountNode
            && this.first instanceof ThenNode
            && this.first.second instanceof FilterNode) {
        optimizeFilters(this.first);
        this.second = new CountIfNode(((this.first as ThenNode).second as FilterNode).predicate);
        this.first = (this.first as ThenNode).first;
        return this;
    } else {
        return null;
    }
});

//// 3.1 cartesian products

export class CartesianProductNode extends ASTNode {
    left: ASTNode;
    right: ASTNode;

    constructor(left: ASTNode, right: ASTNode) {
        super("CartesianProduct");
        this.left = left;
        this.right = right;
    }

    execute(data: any[]): any {
        const cartProduct = [];
        const leftData: any[] = this.left.execute(data);
        const rightData: any[] = this.right.execute(data);
        for (const leftDatum of leftData) {
            for (const rightDatum of rightData) {
                cartProduct.push({"left": leftDatum, "right": rightDatum});
            }
        }
        return cartProduct;
    }
}

//// 3.2-3.6 joins and hash joins

export class JoinNode extends ASTNode {
    left: ASTNode;
    right: ASTNode;
    predicate: (datum: any) => boolean;

    constructor(left: ASTNode, right: ASTNode, predicate: (datum: any) => boolean) {
        super("Join");
        this.left = left;
        this.right = right;
        this.predicate = predicate;
    }

    execute(data: any[]): any {
        const cartProduct = [];
        const leftData: any[] = this.left.execute(data);
        const rightData: any[] = this.right.execute(data);
        for (const leftDatum of leftData) {
            for (const rightDatum of rightData) {
                if (this.predicate({"left": leftDatum, "right": rightDatum})) {
                    cartProduct.push(Object.assign({}, leftDatum, rightDatum));
                }
            }
        }
        return cartProduct;
    }
}

AddOptimization(ThenNode, function(this: ThenNode): ASTNode {
    if (this.second instanceof ApplyNode
            && this.second.callback === ASTNode.combineFn
            && this.first instanceof ThenNode
            && this.first.second instanceof FilterNode
            && this.first.second.predicate["type"] === "function") {
        const cpn = (this.first as ThenNode).first as CartesianProductNode;
        return new JoinNode(
            cpn.left,
            cpn.right,
            this.first.second.predicate
        );
    } else {
        return null;
    }
});

export class HashJoinNode extends ASTNode {
    field: string;
    left: ASTNode;
    right: ASTNode;

    constructor(field: string, left: ASTNode, right: ASTNode) {
        super("HashJoin");
        this.field = field;
        this.left = left;
        this.right = right;
    }

    execute(data: any[]): any {
        const leftData: any[] = this.left.execute(data);
        const rightData: any[] = this.right.execute(data);

        const leftTables = {};
        const rightTables = {};
        for (const leftDatum of leftData) {
            if (!(leftDatum[this.field] in leftTables)) {
                leftTables[leftDatum[this.field]] = [];
            }
            leftTables[leftDatum[this.field]].push(leftDatum);
        }
        for (const rightDatum of rightData) {
            if (!(rightDatum[this.field] in rightTables)) {
                rightTables[rightDatum[this.field]] = [];
            }
            rightTables[rightDatum[this.field]].push(rightDatum);
        }

        const joinedTable = [];
        for (const key in leftTables) {
            if (key in rightTables) {
                for (const leftDatum of leftTables[key]) {
                    for (const rightDatum of rightTables[key]) {
                        joinedTable.push(Object.assign({}, leftDatum, rightDatum));
                    }
                }
            }
        }

        return joinedTable;
    }
}

AddOptimization(ThenNode, function(this: ThenNode): ASTNode {
    if (this.second instanceof ApplyNode
            && this.second.callback === ASTNode.combineFn
            && this.first instanceof ThenNode
            && this.first.second instanceof FilterNode
            && this.first.second.predicate["type"] === "field") {
        const cpn = (this.first as ThenNode).first as CartesianProductNode;
        return new HashJoinNode(
            this.first.second.predicate["field"],
            cpn.left,
            cpn.right
        );
    } else {
        return null;
    }
});
