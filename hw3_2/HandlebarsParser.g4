parser grammar HandlebarsParser;

options { tokenVocab=HandlebarsLexer; }

document : element* EOF ;

element
    : rawElement
    | expressionElement
    | blockElement
    | commentElement
    ;

rawElement
    : TEXT
    | BRACE
    ;

expressionElement : START exp=expression END ;

blockElement : op=openingPart element* cl=closingPart ;

commentElement : START COMMENT END_COMMENT ;

expression
    : lookup
    | literal
    | nestedExpression
    | exprHelper
    ;

lookup : id=ID ;

literal
    : val=INTEGER # Integer
    | val=FLOAT # Float
    | val=STRING # String
    ;

nestedExpression : '(' exp=expression ')' ;

exprHelper : helperApp ;

blockExpression : helperApp ;

helperApp : fn=ID params+=expression*;

openingPart : START BLOCK expr=blockExpression END ;

closingPart : START CLOSE_BLOCK fn=ID END ;
