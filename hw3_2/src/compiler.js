const antlr4 = require("antlr4");
const HandlebarsLexer = require("../gen/HandlebarsLexer").HandlebarsLexer;
const HandlebarsParser = require("../gen/HandlebarsParser").HandlebarsParser;
const HandlebarsParserListener = require("../gen/HandlebarsParserListener").HandlebarsParserListener;

function escapeString(s) {
    return ("" + s).replace(/["'\\\n\r\u2028\u2029]/g, (c) => {
        switch (c) {
            case '"':
            case "'":
            case "\\":
                return "\\" + c;
            case "\n":
                return "\\n";
            case "\r":
                return "\\r";
            case "\u2028":
                return "\\u2028";
            case "\u2029":
                return "\\u2029";
        }
    });
}

class HandlebarsCompiler extends HandlebarsParserListener {
    constructor() {
        super();
        this._inputVar = "_$ctx";
        this._outputVar = "_$result";
        this._helpers = {expr: {}, block: {}};
        this._usedHelpers = {expr: {}, block: {}};

        let __$each = function(ctx, body, list) {
            let result = "";
            for (let i = 0; i < list.length; i++) {
                result += body(list[i]);
            }
            return result;
        };

        let __$if = function(ctx, body, expr) {
            if(expr) {
                return body(ctx);
            } else {
                return "";
            }
        };

        let __$with = function(ctx, body, field) {
            return body(ctx[field]);
        };

        this.registerBlockHelper('each', __$each);
        this.registerBlockHelper('if', __$if);
        this.registerBlockHelper('with', __$with);
    }

    registerExprHelper(name, helper) {
        this._helpers.expr[name] = helper;
    }

    registerBlockHelper(name, helper) {
        this._helpers.block[name] = helper;
    }

    compile(template) {
        this._bodyStack = [];
        this.pushScope();
        const chars = new antlr4.InputStream(template);
        const lexer = new HandlebarsLexer(chars);
        const tokens = new antlr4.CommonTokenStream(lexer);
        const parser = new HandlebarsParser(tokens);
        parser.buildParseTrees = true;
        const tree = parser.document();
        antlr4.tree.ParseTreeWalker.DEFAULT.walk(this, tree);

        let code = this.getHelpers() + this.popScope();
        //console.log(code);
        return new Function(this._inputVar, code);
    }

    pushScope() {
        this._bodyStack.push(`var ${this._outputVar} = "";\n`);
    }

    popScope() {
        return this._bodyStack.pop() + `return ${this._outputVar};\n`;
    }

    getHelpers() {
        let hfns = "";
        for (let helper in this._usedHelpers.expr) {
            hfns += `__$${helper} = ${this._helpers.expr[`${helper}`]};\n`;
            delete this._usedHelpers.expr[helper];
        }
        for (let helper in this._usedHelpers.block) {
            hfns += `__$${helper} = ${this._helpers.block[`${helper}`]};\n`;
            delete this._usedHelpers.block[helper];
        }
        return hfns;
    }

    append(expr) {
        let line = `${this._outputVar} += ${expr};\n`;
        this._bodyStack[this._bodyStack.length - 1] += line;
    }

    exitRawElement(ctx) {
        this.append(`"${escapeString(ctx.getText())}"`);
    }

    exitExpressionElement(ctx) {
        this.append(ctx.exp.source);
    }

    exitExpression(ctx) {
        ctx.source = "";
        let child = ctx.children.pop();
        while (child) {
            ctx.source += child.source;
            child = ctx.children.pop();
        }
    }

    exitFloat(ctx) {
        ctx.source = parseFloat(ctx.getText());
    }

    exitInteger(ctx) {
        let radix = 10;
        let text = ctx.getText();

        if (/^[+-]?0[0-7]*$/.test(text)) {
            radix = 8;
        } else if (/^[+-]?0[Xx][0-9a-fA-F]*$/.test(text)) {
            radix = 16;
        } else if (/^[+-]?0[Oo][0-7]*$/.test(text)) {
            let match = /^([+-]?)0[Oo]([0-7]*)$/.exec(text);
            text = match[1] + match[2];
            radix = 8;
        } else if (/^[+-]?0[Bb][0-1]*$/.test(text)) {
            let match = /^([+-]?)0[Bb]([0-1]*)$/.exec(text);
            text = match[1] + match[2];
            radix = 2;
        }

        ctx.source = parseInt(text, radix);
    }

    exitString(ctx) {
        ctx.source = ctx.getText();
    }

    exitLookup(ctx) {
        ctx.source = `${this._inputVar}.` + ctx.id.text;
    }

    exitNestedExpression(ctx) {
        ctx.source = ctx.exp.source;
    }

    exitExprHelper(ctx) {
        let helperApp = ctx.children.pop();
        this._usedHelpers.expr[helperApp.fn.text] = "";
        ctx.source = `__$${helperApp.fn.text}(_$ctx, `;
        let param = helperApp.params.pop();
        let first = true;
        let paramString = ""
        while (param) {
            if (first) {
                first = false;
            } else {
                paramString = ", " + paramString;
            }
            paramString = param.source + paramString;
            param = helperApp.params.pop();
        }
        ctx.source += paramString + ")";
    }

    enterBlockElement(ctx) {
        this.pushScope();
    }

    exitBlockExpression(ctx) {
        let helperApp = ctx.children.pop();
        ctx.source = {};
        ctx.source.fn = helperApp.fn.text;
        ctx.source.params = helperApp.params;
    }

    exitOpeningPart(ctx) {
        ctx.source = ctx.expr.source;
    }

    exitBlockElement(ctx) {
        let fn = ctx.op.source.fn;
        if (fn !== ctx.cl.fn.text) {
            throw `Block start '${fn}' does not match the block end '${ctx.cl.fn.text}'.`
        }
        this._usedHelpers.block[fn] = "";
        let code = this.popScope();
        let body = new Function(this._inputVar, code);
        let result = `__$${fn}(_$ctx, ${body}, `;
        let params = ctx.op.source.params;
        let param = params.pop();
        let paramString = "";
        let first = true;
        while (param) {
            if (first) {
                first = false;
            } else {
                paramString = ", " + paramString;
            }
            paramString = param.source + paramString;
            param = params.pop();
        }
        result += paramString + ")";
        this.append(result);
    }
}

exports.HandlebarsCompiler = HandlebarsCompiler;
