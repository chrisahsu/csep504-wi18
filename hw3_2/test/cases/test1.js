function three(ctx, body, one, two, three) {
   return "" + one + two + three + body(ctx);
}

function two(ctx, body, one, two) {
   return "" + one + two + body(ctx);
}

function square(ctx, ...params) {
    let result = "";
    for (let i = 0; i < params.length; i++) {
        result += (params[i] * params[i]);
    }
    return result;
}

const ctx = {
    b: "Bee",
};

exports.helpers = [
    ["square", square]
];

exports.blockHelpers = [
    ["two", two],
    ["three", three],
];

exports.ctx = ctx;
exports.description = "Nested blocks with weird expressions and inputs";
