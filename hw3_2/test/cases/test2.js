function a(ctx, param) {
   return param;
}

function b(ctx, body, param) {
   return param;
}

function c(ctx, ...params) {
    let result = "";
    for (let i = 0; i < params.length; i++) {
        result += params[i];
    }
    return result;
}

function d(ctx, body, ...params) {
    let result = "";
    for (let i = 0; i < params.length; i++) {
        result += params[i];
    }
    return result;
}

const ctx = {
};

exports.helpers = [
    ["return", a],
    ["else", c],
];

exports.blockHelpers = [
    ["if", b],
    ["for", d]
];

exports.ctx = ctx;
exports.description = "Helpers with protected terms as names and varying number of params";
