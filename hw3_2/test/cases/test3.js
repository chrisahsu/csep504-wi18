function helperA(ctx, body, ...params) {
   let result = "$A";
   for (let i = 0; i < params.length; i++) {
       result += params[i];
   }
   return result + body(ctx);
}

function helperB(ctx, ...params) {
   let result = "$B";
   for (let i = 0; i < params.length; i++) {
       result += params[i];
   }
   return result;
}

function helperC(ctx, ...params) {
    let result = "$C";
    for (let i = 0; i < params.length; i++) {
        result += params[i];
    }
    return result;
}

const ctx = {
    a: "Ay",
};

exports.helpers = [
    ["helperB", helperB],
    ["helperC", helperC]
];

exports.blockHelpers = [
    ["helperA", helperA]
];

exports.ctx = ctx;
exports.description = "Helpers with mixed types of arguments and nested expressions";
