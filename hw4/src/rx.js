class Stream {
    constructor(stream, stream2, fn) {
        if (fn) {
            this.zipFn = fn;
            this.parents = [stream, stream2];
        } else if (stream2) {
            this.children = [stream, stream2];
        } else if (stream) {
            this.parent = stream;
            this.callbacks = stream.callbacks;
        } else {
            this.base = true;
            this.callbacks = [];
        }
    }

    // Complete the Stream class here.
    subscribe(fn) {
        if (this.base) {
            this.callbacks.push(fn);
        } else if (this.children) {
            for (var i = 0; i < this.children.length; i++) {
                this.children[i].subscribe(fn);
            }
        }  else if (this.parents) {
            var mem = [undefined, undefined];
            var fn2 = (el) => {
                mem[0] = el;
                if (mem[1] !== undefined) {
                    var zipped = this.zipFn(mem[0], mem[1]);
                    fn(zipped);
                }
            };
            this.parents[0].subscribe(fn2);
            fn2 = (el) => {
                mem[1] = el;
                if (mem[0] !== undefined) {
                    var zipped = this.zipFn(mem[0], mem[1]);
                    fn(zipped);
                }
            };
            this.parents[1].subscribe(fn2);
        } else {
            var fn2;
            if (this.isFirst) {
                fn2 = (el) => {
                    if (!fn2.returned) {
                        fn2.returned = true;
                        return fn(el);
                    }
                };
            } else if (this.mapFn) {
                fn2 = (el) => {
                    var mapped = this.mapFn(el);
                    return fn(mapped);
                };
            } else if (this.filterFn) {
                fn2 = (el) => {
                    if (this.filterFn(el)) {
                        return fn(el);
                    }
                };
            } else if (this.isFlatten) {
                fn2 = (el) => {
                    if (el.constructor === Array) {
                        for (var i = 0; i < el.length; i++) {
                            fn(el[i]);
                        }
                    }
                };
            } else if (this.isCombine) {
                fn2 = (el) => {
                    if (el.constructor === Stream) {
                        el.subscribe(fn);
                    }
                };
            } else if (this.throttle) {
                fn2 = (el) => {
                    if (!this.isThrottling) {
                        this.isThrottling = true;
                        setTimeout(() => {
                            fn(el);
                            this.isThrottling = false;
                        }, this.throttle);
                    }
                };
            }
            this.parent.subscribe(fn2);
        }
    }

    first() {
        var stream = new Stream(this);
        stream.isFirst = true;
        return stream;
    }

    map(fn) {
        var stream = new Stream(this);
        stream.mapFn = fn;
        return stream;
    }

    filter(fn) {
        var stream = new Stream(this);
        stream.filterFn = fn;
        return stream;
    }

    flatten() {
        var stream = new Stream(this);
        stream.isFlatten = true;
        return stream;
    }

    join(stream) {
        if (stream === undefined) {
            return this;
        } else {
            return new Stream(this, stream);
        }
    }

    combine() {
        var stream = new Stream(this);
        stream.isCombine = true;
        return stream;
    }

    zip(stream, fn) {
        if (stream === undefined || fn === undefined) {
            return this;
        } else {
            return new Stream(this, stream, fn);
        }
    }

    throttle(n) {
        var stream = new Stream(this);
        stream.throttle = n;
        stream.isThrottling = false;
        return stream;
    }

    _push(el) {
        for (var i = 0; i < this.callbacks.length; i++) {
            this.callbacks[i](el);
        }
    }

    _push_many(arr) {
        for (var i = 0; i < arr.length; i++) {
            this._push(arr[i]);
        }
    }

    static timer(n) {
        var s = new Stream();
        setInterval(() => {
            s._push(new Date());
        }, n);
        return s;
    }

    static dom(el, event) {
        var s = new Stream();
        el.on(event, (ev) => {
            s._push(ev);
        });
        return s;
    }

    static url(url, $) {
        var s = new Stream();
        var f = (json) => {
            s._push(json);
        };
        $.get(url, f, "json");
        return s;
    }
}

if (typeof exports !== "undefined") {
    exports.Stream = Stream;
}

// dependency injection let's do it
function setup($) {
    const FIRE911URL = "https://data.seattle.gov/views/kzjm-xkqj/rows.json?accessType=WEBSITE&method=getByIds&asHashes=true&start=0&length=10&meta=false&$order=:id";

    function WIKIPEDIAGET(s, cb) {
        $.ajax({
            url: "https://en.wikipedia.org/w/api.php",
            dataType: "jsonp",
            jsonp: "callback",
            data: {
                action: "opensearch",
                search: s,
                namespace: 0,
                limit: 10,
            },
            success: function(data) { cb(data[1]); },
        });
    }

    // Add your hooks to implement Part 2 here.
    var s = Stream.timer(1000);
    s.subscribe((x) => {
        $("#time").text(x)
    });

    var i = 0;
    var s2 = Stream.dom($("#button"), "mousedown");
    s2.subscribe((x) => {
        $("#clicks").text(++i);
    });

    var s3 = Stream.dom($("#mousemove"), "mousemove")
        .throttle(1000);
    s3.subscribe((x) => {
        $("#mouseposition").text("(" + x.pageX + ", " + x.pageY + ")");
    });

    var mem = {};
    var fn = (x) => {
        for (var i = 0; i < 10; i++) {
            if (!x[i]) {
                break;
            } else {
                var loc = x[i]['3479077'];
                if (!(loc in mem)) {
                    mem[loc] = "";
                    var srch = $("#firesearch").val();
                    if (loc.includes(srch)) {
                        $("#fireevents").append($("<li></li>").text(loc));
                    }
                }
            }
        }
    };
    var fn2 = () => {
        var s4a = Stream.url(FIRE911URL, $);
        s4a.subscribe(fn);
    };
    fn2();
    var s4 = Stream.timer(60000);
    s4.subscribe(fn2)

    var s5 = Stream.dom($("#wikipediasearch"), "input")
        .throttle(100);
    s5.subscribe((ev) => {
        var f = (x) => {
            $("#wikipediasuggestions").empty();
            for (var i = 0; i < x.length; i++) {
                $("#wikipediasuggestions").append($("<li></li>").text(x[i]));
            }
        };
        WIKIPEDIAGET(ev.target.value, f);
    });

    var s6 = Stream.dom($("#firesearch"), "input");
    s6.subscribe((ev) => {
        $("#fireevents").empty();
        for (var loc in mem) {
            if (loc.includes(ev.target.value)) {
                $("#fireevents").append($("<li></li>").text(loc));
            }
        }
    });
}
