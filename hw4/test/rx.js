const expect = require("chai").expect;
const Stream = require("../src/rx").Stream;

describe("Part 1", () => {
    it("Creating a Stream", () => {
        const out = new Stream();
        expect(out).to.be.an("Object");
    });

    it("Subscribe and _push", () => {
        const elts = [];
        const out = new Stream();
        out.subscribe((x) => { elts.push(x); });
        out._push(1);
        out._push(2);
        out._push(3);
        expect(elts).to.deep.equal([1, 2, 3]);
    });

    it("_push_many", () => {
        const elts = [];
        const out = new Stream();
        out.subscribe((x) => { elts.push(x); });
        out._push_many([1, 2, 3]);
        expect(elts).to.deep.equal([1, 2, 3]);
    });

    it("first", () => {
        const elts = [];
        const out = new Stream();
        out.first().subscribe((x) => { elts.push(x); });
        out._push_many([1, 2, 3]);
        expect(elts).to.deep.equal([1]);
    });

    it("map", () => {
        const elts = [];
        const out = new Stream();
        out.map((x) => { return x % 2; })
            .subscribe((x) => { elts.push(x); });
        out._push_many([1, 2, 3]);
        expect(elts).to.deep.equal([1, 0, 1]);
    });

    it("filter", () => {
        const elts = [];
        const out = new Stream();
        out.filter((x) => { return x % 2 == 1; })
            .subscribe((x) => { elts.push(x) });
        out._push_many([1, 2, 3]);
        expect(elts).to.deep.equal([1, 3]);
    });

    it("flatten", () => {
        const elts = [];
        const out = new Stream();
        out.map((x) => { return Array(x).fill(x) })
            .flatten()
            .subscribe((x) => { elts.push(x); });
        out._push_many([1, 2, 3]);
        expect(elts).to.deep.equal([1, 2, 2, 3, 3, 3]);
    });

    it("join", () => {
        const elts = [];
        const out1 = new Stream();
        const out2 = new Stream();
        out1.join(out2).subscribe((x) => { elts.push(x); });
        out1._push(1);
        out2._push(2);
        out1._push(3);
        expect(elts).to.deep.equal([1, 2, 3]);
    });

    it("combine", () => {
        const elts = [];
        const out = new Stream();
        const stream1 = new Stream();
        const stream2 = new Stream();
        const stream3 = new Stream();
        out.combine()
            .subscribe((x) => { elts.push(x); });
        out._push_many([stream1, stream2, stream3]);
        stream1._push(1);
        stream1._push(2);
        stream2._push(2);
        stream1._push(3);
        stream2._push(3);
        stream3._push(3);
        expect(elts).to.deep.equal([1, 2, 2, 3, 3, 3]);
    });

    it("zip", () => {
        const elts = [];
        const out1 = new Stream();
        const out2 = new Stream();
        out1.zip(out2, (x, y) => { return x * y; })
            .subscribe((x) => { elts.push(x); });
        out1._push(1);
        out2._push(2);
        out1._push(-1);
        out1._push(0);
        out1._push(5);
        out2._push(3);
        out1._push(1);
        out2._push(2);
        expect(elts).to.deep.equal([2, -2, 0, 10, 15, 3, 2]);
    });
});
