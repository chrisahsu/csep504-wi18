grammar Regex;

re
    : regex EOF
    ;

regex
    : expression
    | alt
    ;

alt
    : expression ('|' expression)+
    ;

expression
    : (elems+=element)*
    ;

element
    : a=atom            # AtomicElement
    | a=atom m=MODIFIER # ModifiedElement
    ;

atom
    : c=CHARACTER
    | '(' regex ')'
    ;

CHARACTER : [a-zA-Z0-9.] ;
MODIFIER : [?*] ;
WS : [ \t\r\n]+ -> skip ;
